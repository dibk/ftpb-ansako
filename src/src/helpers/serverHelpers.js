// Helpers
import { getEnvironmentVariable } from 'helpers/environmentVariableHelpers.js';

export const getServerDate = () => {
    const internalApiUrl = getEnvironmentVariable('internalApiUrl');
    const apiUrl = `${internalApiUrl}/ping`
    return fetch(apiUrl).then(res => res?.headers?.get('date'));
}