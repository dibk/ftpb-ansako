import { UPDATE_IS_EDITED } from 'constants/types';

const initialState = false;

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case UPDATE_IS_EDITED:
			return action.payload;
		default:
			return state;
	}
}

export default reducer;
