// Dependencies
import React from "react";
import { useSelector } from "react-redux";

// DIBK Design
import { Button, NavigationBar } from "dibk-design";

// Stylesheets
import style from "components/partials/MainNavigationBar.module.scss";

// Assets
import signOutIcon from "assets/svg/right-from-bracket-solid.svg";

const MainNavigationBar = ({ userManager }) => {
    // Redux store
    const oidc = useSelector((state) => state.oidc);
    const snackbarMessage = useSelector((state) => state.snackbarMessage);
    const selectedSubmission = useSelector((state) => state.selectedSubmission);

    const handleLogoutClick = (event) => {
        event.preventDefault();
        const referanseId = selectedSubmission?.referanseId;
        if (referanseId?.length) {
            sessionStorage.signinRedirectPath = `/skjema/${referanseId}`;
        }
        userManager.signoutRedirect({
            id_token_hint: oidc?.user?.id_token
        });
        userManager.removeUser();
    };

    return (
        <NavigationBar logoLink="/" mainContentId="main-content" >
            {oidc?.user ? (
                <div className={style.rightContainer}>
                    <span className={style.snackbarMessage}>{snackbarMessage}</span>
                    <div className={style.logOutButtonContainer}>
                        <Button color="primary" onClick={handleLogoutClick} noMargin>
                            <img src={signOutIcon} alt="" />
                            <span aria-label="Logg ut">Logg ut</span>
                        </Button>
                    </div>
                </div>
            ) : (
                ""
            )}
        </NavigationBar>
    );
};

export default MainNavigationBar;
