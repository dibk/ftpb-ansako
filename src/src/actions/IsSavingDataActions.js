import { UPDATE_IS_SAVING_DATA } from 'constants/types';

export const updateIsSavingData = (isSavingData) => (dispatch) => {
    dispatch({ type: UPDATE_IS_SAVING_DATA, payload: isSavingData });
}
