import { UPDATE_IS_EDITED } from 'constants/types';

export const updateIsEdited = (isEdited) => (dispatch) => {
    dispatch({ type: UPDATE_IS_EDITED, payload: isEdited });
}
