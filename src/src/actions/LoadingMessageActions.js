import { UPDATE_LOADING_MESSAGE } from 'constants/types';

export const updateLoadingMessage = (loadingMessage) => (dispatch) => {
    dispatch({ type: UPDATE_LOADING_MESSAGE, payload: loadingMessage });
}
