const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = function override(config, env) {
    const configPath = process?.env?.NODE_ENV === 'development' ? 'src/config/config.local.json' : 'src/config/config.json';
    const silentRefreshPath = 'src/silent-refresh.html';
    const oidcClientPath = 'node_modules/oidc-client/dist/oidc-client.min.js';
    config.plugins.push(new CopyWebpackPlugin({
        patterns: [
            { from: configPath, to: 'config.json', toType: 'file' },
            { from: silentRefreshPath, to: 'silent-refresh.html', toType: 'file' },
            { from: oidcClientPath, to: 'oidc-client.min.js', toType: 'file' }
        ]
    }));
    return config;
};
